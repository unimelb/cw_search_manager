# cw_search_manager

A CW search manager written in python, used in O3 analyses. Mostly the LMXB Viterbi searches in mind. Wraps underlying C and/or CUDA codes. Likely to be deprecated before use in O4.

# Running

NOTE: not tested on OzStar 2!! Will likely not run without some tweaking... E.g. Patrick's old lalapps environment.

Old instructions:

First you should load git and anaconda, source patrick clearwater's lalapps environment and then source this python virtualenv.

```bash
module load git/2.18.0
module load anaconda3/5.1.0
. /fred/oz022/lalapps/2018-03/installation/etc/lalapps-user-env.sh
source activate /fred/oz022/xray_gw_work/conda_env/gwxray3
```


