=================
cw_search_manager
=================


.. image:: https://img.shields.io/pypi/v/cw_search_manager.svg
        :target: https://pypi.python.org/pypi/cw_search_manager

.. image:: https://img.shields.io/travis/meyers-academic/cw_search_manager.svg
        :target: https://travis-ci.org/meyers-academic/cw_search_manager

.. image:: https://readthedocs.org/projects/cw-search-manager/badge/?version=latest
        :target: https://cw-search-manager.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




A simple search manager for CW searches...tailored mainly for LMXB Viterbi searches.


* Free software: GNU General Public License v3
* Documentation: https://cw-search-manager.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
