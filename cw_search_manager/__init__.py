# -*- coding: utf-8 -*-

"""Top-level package for cw_search_manager."""

__author__ = """Pat Meyers"""
__email__ = 'pat.meyers@unimelb.edu.au'
__version__ = '0.1.0'
