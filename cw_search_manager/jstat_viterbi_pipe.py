from cw_search_manager.cw_params import Params
import optparse
import pathlib
import os
import logging

# import subprocess as sp
# import numpy as np
# done imports
"""

"""


def parse_command_line():
    """
    parse command line
    """
    parser = optparse.OptionParser()
    parser.add_option(
        "--freq-start",
        help="frequency band start",
        default=None,
        dest="freq_start",
        type=float,
    )
    parser.add_option(
        "--freq-band",
        help="frequency band size",
        default=None,
        dest="freq_band",
        type=float,
    )
    parser.add_option(
        "--config-file",
        help="search manager configuration file",
        default=None,
        dest="config_file",
        type=str,
    )
    parser.add_option(
        "--make-fake-data",
        help="make fake data if this is set and injection flag is in config file. creates atoms.",
        default=False,
        dest="make_fake_data",
        action="store_true",
    )
    parser.add_option(
        "--top-level-directory",
        help="add freqStart to end of this to get directory where things will be saved",
        default="./jstat_atoms",
        dest="top_level_directory",
        type=str,
    )
    parser.add_option(
        "--sft-injection-label",
        help="label for injected SFTs",
        default="INJECTION_SFT",
        dest="sft_injection_label",
        type=str,
    )
    parser.add_option(
        "--cache-files",
        help="comma separated list of cache files",
        default=None,
        dest="cache_files",
        type=str,
    )
    parser.add_option(
        "--sft-files",
        help="string in format 'list:file.txt' that points to file with sfts to use",
        default=None,
        dest="sft_files",
        type=str,
    )
    parser.add_option(
        "--produce-atoms",
        help="add flag to produce fstat atoms",
        default=False,
        dest="produce_atoms",
        action="store_true",
    )
    parser.add_option(
        "--atoms-string",
        help="atoms string, i.e. 'atoms-%d'. Should have %d somewhere.",
        dest="atoms_string",
        default="atoms-%d",
        type=str,
    )
    parser.add_option(
        "--cjs-atoms",
        help="dir where cjs atoms are, with atoms-string at end, i.e. 'atoms-%d'. Should have %d somewhere.",
        dest="cjs_atoms",
        default=None,
        type=str,
    )
    parser.add_option(
        "--out-pref",
        help="prefix for gpu output (can be folder)",
        dest="out_pref",
        default=None,
        type=str,
    )
    parser.add_option(
        "--clean-sfts",
        help="clean SFTs after making injection",
        default=False,
        dest="clean_sfts",
        action="store_true",
    )
    parser.add_option(
        "--run-viterbi-cpu",
        help="run viterbi algorithm",
        default=False,
        dest="run_viterbi_cpu",
        action="store_true",
    )
    parser.add_option(
        "--run-viterbi-gpu",
        help="run gpu code",
        default=False,
        dest="run_viterbi_gpu",
        action="store_true",
    )
    parser.add_option(
        "--gpu-executable",
        help="gpu viterbi executable",
        default=None,
        dest="gpu_executable",
        type=str,
    )
    parser.add_option(
        "--debug", help="debug", default=False, dest="debug", action="store_true"
    )
    parser.add_option(
        "--orbitTp",
        help="Start of orbitTp range",
        default=None,
        dest="orbitTp",
        type=float,
    )
    parser.add_option(
        "--orbitTp-step",
        help="Step size for orbitTp search",
        default=None,
        dest="orbitTp_step",
        type=float,
    )
    parser.add_option(
        "--orbitTp-end",
        help="end of orbitTp range",
        default=None,
        dest="orbitTp_end",
        type=float,
    )
    parser.add_option(
        "--asini", help="Start of asini range", default=None, dest="asini", type=float
    )
    parser.add_option(
        "--asini-step",
        help="Step size for asini search",
        default=None,
        dest="asini_step",
        type=float,
    )
    parser.add_option(
        "--asini-end",
        help="end of asini range",
        default=None,
        dest="asini_end",
        type=float,
    )
    parser.add_option(
        "--rand-seed",
        help="Random seed for MFD repeatability",
        default=None,
        dest="rand_seed",
        type=int,
    )

    cl_params, args = parser.parse_args()
    return cl_params


def check_params(cl_params):
    try:
        cl_params.config_file
    except:
        raise ValueError("Must specify config file")
    try:
        cl_params.freq_start
    except:
        raise ValueError("Must specify frequency band start")


def make_save_dir(cl_params, jstat_params):
    top_level = pathlib.Path(cl_params.top_level_directory)
    print(jstat_params.search.freqStart)
    if cl_params.freq_start is not None:
        print("freq_start")
        save_directory = top_level.joinpath(str(cl_params.freq_start).replace(".", "-"))
    else:
        try:
            if jstat_params.search.freqStart is not None:
                print("freqStart")
                save_directory = top_level.joinpath(
                    str(jstat_params.search.freqStart).replace(".", "-")
                )
        except AttributeError:
            try:
                if jstat_params.injection.fmin is not None:
                    print("fmin")
                    save_directory = top_level.joinpath(
                        str(jstat_params.injection.fmin).replace(".", "-")
                    )
            except AttributeError:
                raise AttributeError(
                    "Need a frequency specified somewhere to make the save_directory!"
                )
    if not os.path.exists(save_directory):
        os.makedirs(save_directory)
        logging.info("Created directory at %s" % str(save_directory))
    logging.info("Save directory set to %s" % str(save_directory))
    return save_directory


def construct_sft_list(cachefiles, start_gps, end_gps):
    """
    Take a list of cache files and create a simple text list of SFTs
    """
    # cachenames = ['IFO','type','gpsStart','duration','path']
    if start_gps is None or end_gps is None:
        print("Must specify minStartTime and maxStartTime in config file")
    sfts = []
    for cache_file in cachefiles:
        with open(cache_file, "r") as myfile:
            for line in myfile:
                ifo, frtype, start, duration, path = line.split()
                # ref:         |------|
                # compare:  |------|
                start = int(start)
                end = int(start) + int(duration)
                if start < start_gps and end > start_gps:
                    # sfts.append(path)
                    sfts.append(remove_prefix(path, "file://localhost"))
                # ref:      |------|
                # compare:    |------|
                elif start < end_gps and end > start_gps:
                    # sfts.append(path)
                    sfts.append(remove_prefix(path, "file://localhost"))
    return sfts


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix) :]
    return text


def run(cl_params):
    if cl_params.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    check_params(cl_params)

    if cl_params.make_fake_data:
        logging.info("Making fake data in atoms format, then exiting")
        jstat_params = Params.from_config_file(cl_params.config_file)
        try:
            jstat_params.injection
        except AttributeError:
            raise AttributeError(
                "Can't make fake data if no injection is specified in param file"
            )

        save_directory = make_save_dir(cl_params, jstat_params)
        sft_save_directory = save_directory.joinpath("sfts")
        sft_save_directory.mkdir(parents=True, exist_ok=True)

        # sft names
        if cl_params.sft_injection_label is not None:
            jstat_params.injection.outLabel = cl_params.sft_injection_label
        elif jstat_params.injection.outLabel is None:
            jstat_params.injection.outLabel = "mfd"
        logging.info("SAVE TAG: %s" % jstat_params.injection.outLabel)

        if cl_params.rand_seed is not None:
            jstat_params.injection.randSeed = cl_params.rand_seed
        # if cl_params.produce_atoms:
        # if we want to produce atoms we have
        # to make the SFTs a bit wider.
        # jstat_params.injection.Band += 0.5
        # jstat_params.injection.fmin -= 0.25
        logging.info(sft_save_directory.joinpath(jstat_params.injection.outLabel))
        jstat_params.injection.outSFTdir = str(sft_save_directory)
        # make injection
        logging.info(jstat_params.injection.injection_command)
        jstat_params.injection.make_gw_injection()

    if cl_params.produce_atoms:
        # produce atoms
        jstat_params = Params.from_config_file(cl_params.config_file)
        if cl_params.freq_start is not None:
            jstat_params.search.freqStart = cl_params.freq_start
        if "loadFstatAtoms" in jstat_params.search:
            del jstat_params.search["loadFstatAtoms"]
        save_directory = make_save_dir(cl_params, jstat_params)
        save_directory.mkdir(parents=True, exist_ok=True)

        if cl_params.atoms_string is not None:
            jstat_params.search.saveFstatAtoms = save_directory.joinpath(
                cl_params.atoms_string
            )
        if jstat_params.search.saveFstatAtoms is None:
            jstat_params.search.saveFstatAtoms = save_directory.joinpath("atoms-%d.dat")
        if "%d" not in str(jstat_params.search.saveFstatAtoms):
            raise NameError("Need a %d in saveFstatAtoms")
        logging.info("Saving F-statistic atoms to...")
        logging.info(jstat_params.search.saveFstatAtoms)

        # get SFT locations
        if cl_params.cache_files is not None:
            # if cache files are supplied let's use them
            sft_locations = cl_params.cache_files.split(",")
            logging.info(
                "Finding SFTs between: %d and %d",
                jstat_params.search.minStartTime,
                jstat_params.search.maxStartTime,
            )
            sfts = construct_sft_list(
                sft_locations,
                jstat_params.search.minStartTime,
                jstat_params.search.maxStartTime,
            )
            sft_file = save_directory.joinpath("sfts_used.txt")
            with open(sft_file, "w+") as myfile:
                for sft in sfts:
                    myfile.write(sft + "\n")
            jstat_params.search.dataFiles = "list:%s" % str(sft_file)
            logging.info("SFTs used saved to:")
            logging.info(sft_file)
        # if sft-files given on CL then overwrite whatever was read in from the .ini
        if cl_params.sft_files is not None:
            jstat_params.search.dataFiles = cl_params.sft_files
        elif jstat_params.search.dataFiles is None:
            # otherwise just search in the save directory.
            raise ValueError("Must supply data files location or string")
        logging.info(jstat_params.search.search_command)
        jstat_params.search.run_gw_search()

    # clean SFTs after making atoms if you want. Otherwise...don't clean them.
    if cl_params.clean_sfts:
        logging.info("CLEANING SFTs")
        jstat_params = Params.from_config_file(cl_params.config_file)

        save_directory = make_save_dir(cl_params, jstat_params)

        sft_save_directory = save_directory.joinpath("sfts")
        jstat_params.injection.outSFTdir = sft_save_directory
        jstat_params.injection.clean_gw_injection()

    if cl_params.run_viterbi_cpu:
        # run viterbi algorithm
        jstat_params = Params.from_config_file(cl_params.config_file)
        if cl_params.freq_start is not None:
            jstat_params.search.freqStart = cl_params.freq_start
        save_directory = make_save_dir(cl_params, jstat_params)
        save_directory.mkdir(parents=True, exist_ok=True)
        if cl_params.atoms_string is not None:
            jstat_params.search.loadFstatAtoms = save_directory.joinpath(
                cl_params.atoms_string
            )
        if jstat_params.search.driftTime != 864000:
            logging.info(
                "!! WARNING !! Drift time set to %d, not 864000, check that the outputs make sense!"
                % jstat_params.search.driftTime
            )
            # raise ValueError('Drift time must be 864000 otherwise CJS will fail')
        if jstat_params.search["loadFstatAtoms"] is None:
            jstat_params.search.loadFstatAtoms = (
                "'" + str(save_directory.joinpath(cl_params.atoms_string)) + "'"
            )
            logging.info("trying to run CPU without specifying atoms location")
            logging.info(
                "setting atoms location to %s" % jstat_params.search.loadFstatAtoms
            )
        if jstat_params.search.dataFiles is None:
            logging.info("dataFiles, although not used, must be set.")
            logging.info('dataFiles is being set to "junk" and wont be used')
            jstat_params.search.dataFiles = "junk"
        # reset jstat parameters
        if cl_params.orbitTp is not None:
            jstat_params.search["orbitTp"] = cl_params.orbitTp
        if cl_params.orbitTp_step is not None:
            jstat_params.search["orbitTp-step"] = cl_params.orbitTp_step
        if cl_params.orbitTp_end is not None:
            jstat_params.search["orbitTp-end"] = cl_params.orbitTp_end
        if cl_params.asini is not None:
            jstat_params.search["asini"] = cl_params.asini
        if cl_params.asini_step is not None:
            jstat_params.search["asini-step"] = cl_params.asini_step
        if cl_params.asini_end is not None:
            jstat_params.search["asini-end"] = cl_params.asini_end
        jstat_params.search.viterbiOutputDirectory = str(save_directory)
        # jstat_params.search.viterbiThreshold = 2
        jstat_params.search.scoresFile = str(save_directory.joinpath("scores.txt"))
        logging.info(jstat_params.search.search_command)
        jstat_params.search.run_gw_search()

    elif cl_params.run_viterbi_gpu:
        jstat_params = Params.from_config_file(cl_params.config_file)
        if cl_params.top_level_directory == "decided_by_ini":
            pass
        else:
            save_directory = make_save_dir(cl_params, jstat_params)
        if cl_params.cjs_atoms is not None:
            jstat_params.search.cjs_atoms = cl_params.cjs_atoms
        if cl_params.out_pref is not None:
            jstat_params.search.out_prefix = cl_params.out_pref
            print(jstat_params.search.out_prefix)
        # let JVP decide various locations, if desired
        if jstat_params.search.cjs_atoms == "let_jvp_decide":
            jstat_params.search.cjs_atoms = str(save_directory.joinpath("atoms-%d"))
        if jstat_params.search.out_prefix == "let_jvp_decide":
            jstat_params.search.out_prefix = str(save_directory.joinpath("results"))
        print(jstat_params.search.search_command)
        jstat_params.search.run_gw_search(True)


if __name__ == "__main__":
    cl_params = parse_command_line()
    run(cl_params)
