#!/bin/python

##################################################
#
#  Pipeline for Viterbi CW search
#     Mostly put together by Meg Millhouse, but
#  borrowed heavily from James Clark's BW pipeline
#  ( https://git.ligo.org/lscsoft/bayeswave )
#
##################################################

from __future__ import print_function

import os
import sys
import shutil
import numpy as np
import subprocess as sp
from cw_search_manager import cw_params

# From BW pipe
from optparse import OptionParser
try:
    import configparser
except ImportError:  # python < 3
    import ConfigParser as configparser

def parser():
    """
    Parser for input (command line and ini file)
    Adapted from bw_pipeline (https://git.ligo.org/lscsoft/bayeswave/blob/master/BayesWaveUtils/bayeswave_pipe/bayeswave_pipe_utils.py)
    """
  
    parser = OptionParser()
    parser.add_option("-r", "--workdir", type=str, default=None)
    parser.add_option("-s", "--subband-idx", type=int, default=0)
      
    (opts,args) = parser.parse_args()
    
    if opts.workdir is None:
        print("ERROR: must specify --workdir", file=sys.stderr)
        sys.exit(1)
              
    if len(args)==0:
        print("ERROR: require config file", file=sys.stderr)
        sys.exit(1)
    if not os.path.isfile(args[0]):
        print("ERROR: config file %s does not exist"%args[0], file=sys.stderr)
        sys.exit(1)
                          
    return opts, args

def make_viterbi_command_line(ini_params, Nstep, workdir):
    '''
        Makes the Viterbi command line.
    '''
    
#     if not os.path.exists(ini_params.viterbi.outputDir):
#         os.mkdir(ini_params.viterbi.outputDir)
    
    cmd = "{0} {1} ".format(ini_params.viterbi.ldas_matlab_init, ini_params.viterbi.viterbi_executable)
    cmd += "{0} {1} {2} {3}".format(ini_params.viterbi.subbandIdx, workdir+'/', 'viterbi_score.txt', Nstep)

    return cmd

def fill_missing_Fstat(ini_params,):
    '''
        Makes stand in Fstat files when there is no data available for the Tdrift segment. Uses a constant likelihood of "4.0" because that's what Patrick used.
    '''
    f = ini_params.search.Freq
    bw = ini_params.search.FreqBand
    
    if ini_params.search.dFreq:
        df = ini_params.search.dFreq
    else:
        # I think in main I already explicitly set this. I'll keep it here just in case.
        df = 1./(2.0*ini_params.viterbi.Tdrift)

    fmax = f+bw

    outfile = open(ini_params.search['outputFstat'],'w')
    
    # Write header for stand-in Fstat file
    outfile.write("%% cmdline: dummy command line for missing SFTs\n")
    outfile.write("%% GPS starttime         = %f\n \
%% Total time spanned    = %f s\n \
%% Pulsar-params refTime = %f\n \
%% Spin-range at refTime: fkdot = [ %f:%f, 0:0, 0:0, 0:0, 0:0, 0:0, 0:0 ]\n \
%% columns:\n \
%% freq alpha delta f1dot f2dot f3dot 2F\n" % (ini_params.search['minStartTime'], ini_params.viterbi['Tdrift'], ini_params.search['minStartTime'], f, fmax))


    while f < fmax:
    # TODO: Might want to get rid of stand ins for f1dot, f2dot, etc
        outfile.write("%.13f %s %s %f %f %f %f\n" % (f, ini_params.search['Alpha'], ini_params.search['Delta'], 0, 0, 0, 4.0))
        f = f+df

    outfile.close()

# TODO: should this be part of the Fstat class? And, would it be more efficient to load the cache files up once only? (yes, probably)
def find_SFTs(ini_params):
    '''
        If an SFT cache is given, makes the string of relevant SFT files to feed to ComputeFstat.  This makes the Fstat calculation a lot faster since it doesn't have to go through an entire run's worth of SFTs.
    '''
    
    if ini_params.search.DataFiles:
        print("Warning! Overriding given DataFiles argument.\n")
    
    sftStr = '\"'

    for key in ini_params.viterbi.SFTCache:
        cache_data = ini_params.viterbi.SFTCache[key]

        try:
            # TODO: maybe clean this up a bit
            indlow = np.max((0,np.where(ini_params.search.minStartTime<cache_data['gpsStart'])[0][0]-1))
            indhi = np.min((len(cache_data),np.where(ini_params.search.maxStartTime>cache_data['gpsStart'])[0][-1]+1))
            
            for k in range(indlow,indhi):
                sftpath = cache_data[k]['path'][16:] # The '16' gets rid of junk ahead of the file path
                sftStr += sftpath+';'

        except:
            print('No SFTs found in %s for this time period.' % key)

    sftStr = sftStr.strip(';') # get rid of trailing semicolon.
    sftStr += '\"' # close the quotation marks!

    # Feed this back to the command line
    ini_params.search.DataFiles = sftStr

#################
#      MAIN     #
#################

# Read in arguments
opts, args = parser()

inifile = args[0]
workdir = os.path.abspath(opts.workdir)
subbandIdx = opts.subband_idx


# Check workdir, and make workdir
if os.path.exists(workdir):
    print("WARNING! Directory %s already exists.\n" % workdir)
    workdir = workdir+'_1'
    print("Changing to %s.\n" % workdir)
os.makedirs(workdir)
shutil.copy(inifile,workdir)
os.chdir(workdir)

# Parse ini file
ini_params = cw_params.Params.from_config_file(inifile)

# First, check for Viterbi parameters
if not hasattr(ini_params,'viterbi'):
    print("Error! Need to supply parameters for Viterbi search.\n")
    sys.exit(1)

# Check to see if we're doing injections
if hasattr(ini_params,'injection'):
    injection_flag = True
else:
    injection_flag = False

# Run injections if necessary
if injection_flag:
    
    # Check to see if there's more that one IFO
    ifoList = ini_params.injection.IFO.split(',')
    if len(ifoList) > 1:
        for ifo in ifoList:
            ini_params.injection.IFO = ifo
            if ini_params.injection.outSingleSFT.lower() == 'true':
                ini_params.injection.outSFTbname = '%s.sft' % ifo
            print("Making SFTs for injection in %s..." % ifo)
            ini_params.injection.make_gw_injection()
    else:
        print("Making SFTs for injection...")
        ini_params.injection.make_gw_injection()
        
    print("...done\n")
    
    # Now make a cache
    if ini_params.injection.outSingleSFT.lower() == 'false':
#         ini_params.viterbi.SFTCacheFiles = []
        ini_params.viterbi.SFTCache = {}
        for ifo in ifoList:
            cachenames = ['IFO','type','gpsStart','duration','path']
            key = 'IFO%s' % ifo
            
            make_cache_cmd = 'ls *%s*.sft | lalapps_path2cache > %s.cache' % (ifo,ifo)
            os.system(make_cache_cmd)
            
            try:
                ini_params.viterbi.SFTCache[key] = np.recfromtxt("%s.cache" % ifo, names=cachenames, encoding=None)
            except:
                # Python2
                ini_params.viterbi.SFTCache[key] = np.recfromtxt("%s.cache" % ifo, names=cachenames)
            
    
    # TODO: add a bit to get rid of SFTs automatically after running Fstat etc

# Set up for sub-bands:
ini_params.viterbi.subbandIdx = subbandIdx


start_Freq = ini_params.search.Freq # this is the starting frequency of the *whole* search
ini_params.search.Freq = start_Freq + ini_params.viterbi.subbandIdx*(ini_params.viterbi.subbandWidth - ini_params.viterbi.subbandOffset)

print("Running on subband index %i. The starting frequency for this search is %f\n" % (ini_params.viterbi.subbandIdx, ini_params.search.Freq))
    
# Now actually run the search


print("Running search on chunks of Tdrift = %g s\n" % ini_params.viterbi.Tdrift)

# Read some of the settings out of the viterbi info
Nstep = ini_params.viterbi.Nsteps
searchStartTime = ini_params.search.minStartTime
Tdrift = ini_params.viterbi.Tdrift

# Need to specify deltaF in terms of Tdrift
ini_params.search.dFreq = 1./(2.*Tdrift)

Fstat_stream_out = open("Fstat.out","w")

missing_data_counter = 0

# TODO: min and max gps times not required in cw_params. Should they be required in the Search section or in Viterbi section?
for i in range(Nstep):
    # Define start and end time for current Tdrift chunk
    ini_params.search.minStartTime = searchStartTime + i*Tdrift
    ini_params.search.maxStartTime = searchStartTime + (i+1)*Tdrift
    #print(ini_params.search['minStartTime'],ini_params.search['maxStartTime'])
    
    # Find SFTs if given a cache
    if ini_params.viterbi.SFTCache:
        find_SFTs(ini_params)
    
    ini_params.search['outputFstat'] = "Fstat-{0}-{1}.dat".format(ini_params.viterbi.subbandIdx,i)
    
    # Check if cache file for SFTs was found
    

    # Run Fstat
    print("Running Fstat for step number %i...\n" % i)
    #print(ini_params.search.search_command)
    ini_params.search.run_gw_search(outputfile=Fstat_stream_out)
    print("done.\n")

    # Now check to see if we need to fill in missing chunk
    if not os.path.exists(ini_params.search['outputFstat']):
        print("No Fstat file for step number %i. Filling in with constant likelihood\n" % i)
        fill_missing_Fstat(ini_params)
        missing_data_counter += 1

Fstat_stream_out.close()

print("Filled in %i Fstat chunks." % missing_data_counter)

# Make Viterbi command line
viterbi_cmd = make_viterbi_command_line(ini_params, Nstep, workdir)

# Call viterbi command line
# TODO: this probably only works with my specific version of Viterbi.  Should find a way to generalize this as much as possible.
#print(viterbi_cmd)
os.system(viterbi_cmd)

# Clean up Fatat files if necessary
if ini_params.viterbi.cleanFstat:
    print("\nRemoving Fstat files...")
    sp.call("rm Fstat*dat",shell=True)
    print("done.\n")

    
# Clean up injection SFTs if they exist
if injection_flag:
    print("Removing injection SFTs...")
    sp.call("rm *.sft",shell=True)
    print("done.\n")
    
print("---------\n")
print("Complete.\n")
print("---------\n")














