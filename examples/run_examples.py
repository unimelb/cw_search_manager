from cw_search_manager import cw_params
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('/fred/oz022/xray_gw_work/code_repositories/gwxray/gwxray/static/meyers.mplstyle')

#######
# run isolated F-stat
#######

isolated_fstat = cw_params.Params.from_config_file('isolated_pulsar.ini')
isolated_fstat.injection.make_gw_injection()
isolated_fstat.search.run_gw_search()

# load output f-stat
results = np.loadtxt(isolated_fstat.search.outputFstat, comments='%')

annotation='$h_0$ = %1.1e\n$\\sqrt{\\rm Noise PSD }$ = %1.1e\nDuration = %4.2f days' % (
        isolated_fstat.injection.h0,
        isolated_fstat.injection.noiseSqrtSh,
        isolated_fstat.injection.duration / 86400)

# plot output f-stat
plt.figure(figsize=(9,6))
plt.title('Isolated neutron star signal')
plt.plot(results[:, 0], results[:, -1])
plt.text(100.0025, 70, annotation, fontsize=16)
plt.xlim(99.975, 100.025)
plt.xlabel('Frequency [Hz]')
plt.ylabel('Isolated NS detection statistic')
plt.savefig('fstat_isolated.png', dpi=600)


#######
# run binary F-stat
#######

binary_fstat = cw_params.Params.from_config_file('binary_pulsar_fstat.ini')
binary_fstat.injection.make_gw_injection()
binary_fstat.search.run_gw_search()

# load output f-stat
results = np.loadtxt(binary_fstat.search.outputFstat, comments='%')
annotation='$h_0$ = %1.1e\n$\\sqrt{\\rm Noise PSD }$ = %1.1e\nDuration = %4.2f days' % (
        binary_fstat.injection.h0,
        binary_fstat.injection.noiseSqrtSh,
        binary_fstat.injection.duration / 86400)

# plot output f-stat
plt.figure(figsize=(9,6))
plt.title('Binary neutron star signal [doppler spreading]')
plt.plot(results[::10, 0], results[::10, -1])
plt.text(100.00, 100, annotation, fontsize=16)
plt.xlim(99.975, 100.025)
plt.xlabel('Frequency [Hz]')
plt.ylabel('Isolated NS detection statistic')
plt.savefig('fstat_binary.png', dpi=600)

binary_jstat = cw_params.Params.from_config_file('binary_pulsar_jstat.ini')
binary_jstat.injection.make_gw_injection()
print(binary_jstat.search.search_command)
binary_jstat.search.run_gw_search()

# load output f-stat
results = np.loadtxt(binary_jstat.search.outputJstat, comments='%',skiprows=3)
annotation='$h_0$ = %1.1e\n$\\sqrt{\\rm Noise PSD }$ = %1.1e\nDuration = %4.2f days' % (
        binary_jstat.injection.h0,
        binary_jstat.injection.noiseSqrtSh,
        binary_jstat.injection.duration / 86400)

# plot output f-stat
plt.figure(figsize=(9,6))
plt.title('Binary neutron star signal [doppler spreading]')
plt.plot(results[:, 1], results[:, 0])
plt.text(100.0025, 150, annotation, fontsize=16)
plt.xlim(99.975, 100.025)
plt.xlabel('Frequency [Hz]')
plt.ylabel('Binary detection statistic')
plt.savefig('jstat_binary.png', dpi=600)
