# !/bin/bash

#####################################################
# Some useful ways of running `jstat_viterbi_pipe.py`
# One should not feel like this needs to be edited.
#####################################################



###############################################
# produce fake sfts
###############################################
python ../cw_search_manager/jstat_viterbi_pipe.py --config-file test_jstat_pipe.ini \
                             --top-level-directory 'test/' \
                             --make-fake-data

# produce a cache file to pass to atom maker
ls test/*/sfts/* | lalapps_path2cache > mycache.lcf

###############################################
# produce atoms from fake data with cache files
###############################################
python ../cw_search_manager/jstat_viterbi_pipe.py \
                             --config-file test_jstat_pipe.ini \
                             --top-level-directory 'test/' \
                             --cache-files 'mycache.lcf' \
                             --produce-atoms \
                             --run-viterbi-cpu


###############################################
# run viterbi from cjs
###############################################
# echo 'running cpu code' viterbi
python ../cw_search_manager/jstat_viterbi_pipe.py --config-file test_jstat_pipe.ini \
                                                  --top-level-directory 'test/' \
                                                  --run-viterbi-cpu
#
# echo 'running gpu code'
# ###############################################
# # Run GPU code, but you need to supply the gpu executable
# # if you want to do this.
# ###############################################
# python ../cw_search_manager/jstat_viterbi_pipe.py --config-file=test_jstat_pipe_gpu.ini \
#                              --top-level-directory='test/' \
#                              --orbitTp=1238166343 \
#                              --orbitTp-step=1 \
#                              --orbitTp-end=123816636 \
#                              --asini=2.35 \
#                              --asini-step=0.01 \
#                              --asini-end=2.45 \
#                              --gpu-executable='/fred/oz022/AFV_test/O3_Threshold/viterbi/viterbi_cuda/viterbi_jstat_search_single' \
#                              --run-viterbi-gpu
#
