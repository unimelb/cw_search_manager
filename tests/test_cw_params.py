#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `cw_search_manager` package."""

import pytest
import os
# from gwpy.timeseries import TimeSeries


from cw_search_manager import cw_params
import numpy.testing as npt

# define some default instances
# of things

# default injection params for all tests
@pytest.fixture
def test_apply_metric_params():
    params = {
        "search_type": "jstatistic",
        "alpha": 0,
        "delta": 0,
        "freqBand": 1,
        "freqStart": 100,
        "dataFiles": "tests/sft*",
        "orbitTp": 1164543014,
        "orbitTp-end": 1164543614,
        "orbital-P": 68023.86,
        "asini": 1.450,
        "asini-end": 3.250,
        "driftTime": 86400,
        "outputMode": "full",
        "saveFstatAtoms": "tests/atoms-%d.dat",
        "minStartTime": 1164600922,
        "maxStartTime": 1164600922+86400
    }
    return params


@pytest.fixture
def test_injection_params():
    params = {
        "IFO": "H1",
        "Alpha": 0,
        "Delta": 0,
        "h0": 1e-25,
        "refTime": 1164600922,
        "startTime": 1164600922,
        "duration": 86400,
        "Freq": 100,
        "Band": 4,
        "fmin": 98,
        "outSFTbname": "tests/sft.sft",
    }
    return params


# default fstat params for all tests
@pytest.fixture
def test_fstat_params():
    params = {
        "Alpha": 0,
        "Delta": 0,
        "FreqBand": 2,
        "Freq": 99,
        "outputFstat": "tests/results.fstat",
        "DataFiles": "tests/*.sft",
    }
    return params


@pytest.fixture
def test_jstat_params():
    params = {
        "search_type": "jstatistic",
        "alpha": 0,
        "delta": 0,
        "freqBand": 2,
        "freqStart": 99,
        "dataFiles": "tests/sft*",
        "driftTime": 86400,
        "outputMode": "full",
        "saveFstatAtoms": "tests/atoms-%d.dat",
        "minStartTime": 1164600922,
        "maxStartTime": 1164600922+86400
    }
    return params

@pytest.fixture
def test_sft_params():
    params = {'gps-start-time': 1247961618,
              'gps-end-time': 1247968818,
              'type' : 'L1_HOFT_C00',
              'observatory': 'L',
              'frame-cache': 'tests/l1cache.cache',
              'high-pass-freq': 15,
              'sft-duration': 1800,
              'start-freq': 20,
              'band': 20,
              'channel-name': 'L1:GDS-CALIB_STRAIN',
              'sft-write-path': 'tests/',
              'dq-flags': 'L1:DMT-ANALYSIS_READY:1'
              }
    return params

# default ini file for all tests
@pytest.fixture
def default_ini_file():
    return "tests/test_config.ini"


# On to the actual tests...
def test_injection_params_print_defaults():
    """
    try printing defaults
    """
    cw_params.InjectionParams.print_defaults()


def test_injection_params_initialize_defaults():
    """
    try initializing default injection parameters
    """
    testval = cw_params.InjectionParams()
    print(testval)
    cmd = testval.injection_command
    print(cmd)


def test_make_injection(test_injection_params):
    """
    try making an injection using default
    injection parameters
    """
    test = cw_params.InjectionParams(**test_injection_params)
    test.make_gw_injection()
    test.clean_gw_injection()


def test_load_config(default_ini_file):
    """
    load config, make sure it has the correct values
    """
    params = cw_params.Params.from_config_file(default_ini_file)
    npt.assert_equal(params.injection.h0, 1e-25)
    npt.assert_equal(params.injection.noiseSqrtSh, 1e-24)
    npt.assert_equal(params.injection.duration, 86400)
    npt.assert_equal(params.injection.fmin, 98)
    npt.assert_equal(params.injection.Band, 4)
    npt.assert_equal(params.injection.startTime, 1164600922)
    # call string representation just to make sure it works.
    print(params)


def test_search_params_print_defaults(test_injection_params):
    """
    test search params defaults
    """
    cw_params.SearchParams.print_defaults()
    cw_params.SearchParams.print_defaults(search_type="jstatistic")
    cw_params.SearchParams.print_defaults(search_type="fstatistic")
    test = cw_params.SearchParams()
    print(test)
    test = cw_params.SearchParams({"search_type": "jstatistic"})
    print(test)

def test_make_sft_cache(test_sft_params):
    params = cw_params.Params()
    params.l1cache = cw_params.SFTParams(**test_sft_params)
    print(params.l1cache.cache_file_command)
    params.l1cache.make_cache_file()
    assert(os.path.isfile(params.l1cache['frame-cache']))
    print(params.l1cache.make_sft_plan())
    params.l1cache.make_sfts()
    os.system('rm {0}'.format(params.l1cache['frame-cache']))
    os.system('rm tests/*.sft')


def test_make_injection_and_recover(
    test_fstat_params, test_injection_params, default_ini_file, test_jstat_params
):
    """
    make and recover injection, then clean up
    # params.injection = cw_params.InjectionParams(**test_injection_params)
    """
    params = cw_params.Params()
    params.injection = cw_params.InjectionParams(**test_injection_params)
    params.search = cw_params.SearchParams(**test_fstat_params)
    params.injection.make_gw_injection()
    params.search.run_gw_search()
    params.injection.clean_gw_injection()
    params.search.clean_gw_search()

    # test jstat version
    params = cw_params.Params()
    params.injection = cw_params.InjectionParams(**test_injection_params)
    params.search = cw_params.SearchParams(**test_jstat_params)
    params.injection.make_gw_injection()
    params.search.run_gw_search()
    params.injection.clean_gw_injection()
    params.search.clean_atoms()

    # Test running from config file
    params = cw_params.Params.from_config_file(default_ini_file)
    params.injection.make_gw_injection()
    params.search.run_gw_search()
    params.injection.clean_gw_injection()
    params.search.clean_gw_search()

# def test_get_strain_timeseries(test_injection_params):
#     params = cw_params.Params()
#     params.injection = cw_params.InjectionParams(**test_injection_params)
#     data = params.injection.get_gw_strain(1)
#     print(data)
#     assert(type(data) == TimeSeries)

def test_apply_metric(test_apply_metric_params):
    params = cw_params.Params()
    params.search = cw_params.SearchParams(**test_apply_metric_params)
    params.search.apply_metric()
    npt.assert_almost_equal(params.search['asini-step'], 0.0014150943396226416)
    npt.assert_almost_equal(params.search['orbitTp-step'], 4.6875)

